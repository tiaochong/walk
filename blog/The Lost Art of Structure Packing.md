http://www.catb.org/esr/structure-packing/

# Summary

- What is the problem the paper/lecture is trying to solve?
- What are the key ideas of the paper? Key insights?
- What are the key mechanisms? What is the implementation?
- What are the key results? Key conclusions?

# Strengths

- How is the problem solved

# Weaknesses / Extension

# Improvements

# Takeaways

# Comments

"{\"action\":\"A\",\"referrerId\":318,\"memberId\":344,\"retryCount\":0,\"orderId":241}"
