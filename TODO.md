
当面试研究生时，我期望他／她满足优秀本科生的能力之外，具有研究能力。

所谓研究能力，是指了解某个领域的发展过程，可以自行搜寻、阅读、理解学术及业界的文献／演讲，能动手编码复现一些文献中记载的技术，分析各种技术之间的优劣。理想地当然是能做出一些技术创新。面试期间能对研究领域外的面试官简单清楚地解释自己做过的研究课题，当中的难点及解决方法。

现阶段的学习方法, 以及未来的学习方法, 任意找一个idea, 把它实现为代码, lifelong leraning

# day section

- write good code

  1.  [effective java]
      > every java programmer should read 10 times
  2.  [a little book on semphore]
      > concurrent programming
  3.  [cs190](http://www.stanford.edu/~ouster/cgi-bin/cs190-winter19/index.php)
      > great course on write better code
  4.  6.033
      > build large systems
  5.  code complete
  6.  programming practice

- Across the abstraction

  1. cpu? alu?
     - **core** [6004](https://6004.mit.edu/web/fall18/resources)
     - [cs233](https://wiki.illinois.edu/wiki/display/cs233fa17/Info)
       - [lab](https://github.com/k98kumar/CS233)
     - [eth]()
     - [http://users.ece.utexas.edu/~patt/]()
  2. computer
     - [core](https://www.elsevier.com/books-and-journals/book-companion/9780128119051)
        > understand the book and all prerequist.
     - [6.823](http://csg.csail.mit.edu/6.823)
     - [18447](http://www.archive.ece.cmu.edu/~ece447/s15/)
     - [18740]
     - https://www.coursera.org/learn/comparch
     - [eth]
     - [http://users.ece.utexas.edu/~patt/]()
     - [15418](https://www.cs.cmu.edu/afs/cs/academic/class/15418-s18/www/)
     - https://courses.cs.washington.edu/courses/cse548/18sp/

- write difficult code
  1. 6.006 + [295](https://contest.cs.cmu.edu/295/f18/)
  2. 6.042 + 6.046
  3. [6.854](http://people.csail.mit.edu/moitra/854.html)
  4. [6.851](http://courses.csail.mit.edu/6.851/fall17/lectures/)
  5. Opendata structure -> uiuc book
  6. cmu course

# night

- cpp
  - google code style guide -> effecitve c++, effective modern c++ -> effective stl -> more effecitve c++
  - abseil -> folly -> etc
  - 暂定每周日花时间review这周所有的代码, 该重构的重构该改的改.
  - 每周六用来补充知识, 只看书不写代码
- system programming
  - cs106x + cs106l + cs107 + cs110 + 15-213
- ds
  - 6.824 + 15 440 + yfs
- net
  - dartnet + Stanford cs144
- os
  - 6.828 + cs140e + cs140 + [cs9242]
  - (https://www.cse.unsw.edu.au/~cs9242/17/project/index.shtml) + 15712 + 15410
  - http://www.cs.utexas.edu/~vijay/cs378-f17/index.htm
  - http://www.cs.utexas.edu/~vijay/cs380D-s18/index.htm
- db
  -15445 & 6.830 > 15721 & cs346 > uw cs525
  - http://pages.cs.wisc.edu/~jignesh/cs764/CS764-F17.html
- big data system
  - http://www.cs.cmu.edu/~15719/readinglist.html
-

