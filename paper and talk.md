# Summary

- What is the problem the paper/lecture is trying to solve?
- What are the key ideas of the paper? Key insights?
- What are the key mechanisms? What is the implementation?
- What are the key results? Key conclusions?

# Strengths

- How is the problem solved

# Weaknesses / Extension

# Improvements

# Takeaways

# Comments

1. Summary

- What is the problem the paper/lecture is trying to solve?
- What are the key ideas of the paper? Key insights?
- What are the key mechanisms? What is the implementation?
- What are the key results? Key conclusions?

2. Strengths (most important ones)
   Does the paper solve the problem well? Is it well written?

3. Weaknesses (most important ones) / Extension (Has others did better?)
   This is where you should think critically. Every paper/idea has a weakness. This does not mean the paper is necessarily bad. It means there is room for improvement and future research can accomplish this.

4. Can you do (much) better? Present your thoughts/ideas.

5. Takeaways: What you learned/enjoyed/disliked? Why?

6. Any other comments you would like to make.
