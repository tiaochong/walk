* init
  * sem = Semaphore(1)
* operation
  * increment vs decrement
  * signal vs wait -> better
  * V vs P
* usage
  * mutex
    * mutex = Semaphore(1) 
    * mutex.wait() -> lock
    * mutex.signal() -> unlock
  * Barrier
    * 