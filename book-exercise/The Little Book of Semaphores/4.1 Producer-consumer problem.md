# description

```
producer THREAD:

	event = waitForEvent()
	buffer.add(event)

consumer THREAD:

	event = buffer.get()
	event.process()
```

## Problems:

1. buffer concurrent get and add
2. get should block when empty
3. add should block when full
4. use only

## Attempt 1: Semaphores (Fail)

```
	lock = sem(1)
	bufferFull = sem(0)
	bufferEmpty = sem(0)
```

```
	event.waitForEvent()
	bufferFull.wait()
	lock.wait()
	buffer.add(event)
	bufferEmpty.signal()
	lock.signal()
```

```
	bufferEmpty.wait()
	lock.wait()
	event = buffer.get()
	bufferFull.signal()
	lock.signal()
	event.process()
```

## Attempt 2: with conditional variable

# Summary

1. lock is used to protect data, not the code
