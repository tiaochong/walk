- memory bank
  - https://en.wikipedia.org/wiki/Memory_bank
  - a bank consists of multiple rows and columns of storage units
  - In a single read or write operation, only one bank is accessed
- vector
  - swap
    - https://stackoverflow.com/questions/6224830/c-trying-to-swap-values-in-a-vector
    - std::swap(v[0],v[1]);
    - std::iter_swap(v.begin(),v.begin()+1);
  - iterator
    - https://stackoverflow.com/questions/2395275/how-to-navigate-through-a-vector-using-iterators-c
      - begin
      - end
      - vector
    - ## modify while visit
    - reverse
      - https://stackoverflow.com/questions/8877448/how-do-i-reverse-a-c-vector
      - std::reverse(ve.begin(), ve.end())
  - ## random access iterator
  - destructor
    - order
      - https://docs.microsoft.com/en-us/cpp/cpp/destructors-cpp?view=vs-2017
  - std::sort

1. stddef
   1. http://www.cplusplus.com/reference/cstddef/
   2. 提供 size_t, nullptr
2. stdint
   1. http://www.cplusplus.com/reference/cstdint/
   2. 提供 int8_t, int32_t
3. explicit
4. constexpr
5. c++ atomic int
   https://stackoverflow.com/questions/31978324/what-exactly-is-stdatomic
   https://gcc.gnu.org/onlinedocs/gcc-4.4.3/gcc/Atomic-Builtins.html
   https://stackoverflow.com/questions/45637962/c-atomic-increment-with-memory-ordering
   http://www.cplusplus.com/reference/atomic/atomic/atomic/
6. Memory_ordering
   1. http://en.wikipedia.org/wiki/Memory_ordering
