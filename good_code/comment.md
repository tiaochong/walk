# core

help understand the code.

# key

- modify code must modify comment
- describe non-obvious things
  - Lower level comments provide precision (especially for variables, arguments, return values):
    - Exactly what is this thing?
    - What are the units?
    - Boundary conditions
      - Does "end" refer to the last value, or the value after the last one?
      - Is a null value allowed? If so, what does it mean?
    - If memory is dynamically allocated, who is responsible for freeing it?
    - Invariants?
  - Higher-level comments capture intuition:
    - Abstractions: a higher-level description of what the code is doing.
    - Rationale for the current design: why the code is this way.
    - How to choose the value of a configuration parameter.
- distinct Interface comment and Implementation comment
  - Interface: what someone needs to know in order to use this class or method
  - Implementation: how the method or class works internally to implement the advertised interface.
  - Important to separate these: do not describe the implementation in the interface documentation
- interface documention
  - Put immediately before the class or method declaration
  - Goal: describe the abstraction (create simple, intuitive model for users)
  - Simpler is better
  - Complete: must include everything that any user might need to know
  - Interface description may use totally different terms than the implementation (if they are simpler)
- implementation documention
  - For many methods, not needed 
  - For longer methods, document major blocks of code 
    - Describe what's happening at a higher level 
    - E.g., what does each loop iteration do? 
  - Document tricky aspects, non-obvious reasons for code 
  - Document dependencies ("if you change this, you better also...") 
  - Documenting variables is less important for method local variables (can see all of the uses), but sometimes needed for longer methods or tricky variables. 
# reference

- https://web.stanford.edu/~ouster/cgi-bin/cs190-winter18/lecture.php?topic=comments
-
